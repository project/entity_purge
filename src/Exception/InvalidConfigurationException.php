<?php

namespace Drupal\entity_purge\Exception;

/**
 * Exception when invalid or insufficient configuration has been provided.
 */
class InvalidConfigurationException extends \Exception {
}
