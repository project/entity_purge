<?php

namespace Drupal\entity_purge;

/**
 * Defines the interface for the purger service.
 *
 * The purger service is responsible for running purge operations. A "scheduler"
 * must call the purger to run the purge operations. Running purge operations
 * via Cron is supported by this module.
 */
interface PurgerInterface {

  /**
   * Runs a purge operation for each of the given purge types.
   *
   * @param \Drupal\entity_purge\Entity\PurgeTypeInterface[] $purge_types
   *   An array containing the purge types for which to run purge operations.
   */
  public function run(array $purge_types): void;

}
