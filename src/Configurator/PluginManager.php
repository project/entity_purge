<?php

declare(strict_types=1);

namespace Drupal\entity_purge\Configurator;

// Drupal modules.
use Drupal\entity_purge\Annotation\EntityPurgeConfigurator as PluginAnnotation;
use Drupal\entity_purge\Entity\PurgeTypeInterface;
// Drupal core.
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The default implementation of the manager for purge configurator plugins.
 *
 * @see \Drupal\entity_purge\Annotation\EntityPurgeConfigurator
 * @see \Drupal\entity_purge\Configurator\PluginInterface
 * @see plugin_api
 */
class PluginManager extends DefaultPluginManager implements
  PluginManagerInterface {

  /**
   * Constructs a new PluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/EntityPurge/Configurator',
      $namespaces,
      $module_handler,
      PluginInterface::class,
      PluginAnnotation::class
    );

    $this->alterInfo('entity_purge_purge_configurator_info');
    $this->setCacheBackend(
      $cache_backend,
      'entity_purge_purge_configurator',
      ['entity_purge_purge_configurator_plugins'],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createInstanceForPurgeType(
    PurgeTypeInterface $purge_type,
  ): PluginInterface {
    return $this->createInstance(
      $purge_type->getPluginId(),
      $purge_type->getPluginConfiguration() ?? [],
    );
  }

}
