<?php

namespace Drupal\entity_purge\Configurator;

// Drupal modules.
use Drupal\entity_purge\Entity\PurgeTypeInterface;
// Drupal core.
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provides the interface for purge configurator plugins.
 *
 * Purge configurator plugins are responsible for configuring the behavior
 * of purge types. Their responsibilities currently includes:
 * - Determine how/when the purger runs a purge of a specific type.
 * - Collect the entities that will be purged.
 * - Cancel purges of individual entities after they are collected for purging.
 */
interface PluginInterface extends
  ConfigurableInterface,
  DependentPluginInterface,
  PluginFormInterface,
  PluginInspectionInterface {

  /**
   * Returns the plugin label.
   *
   * @return string
   *   The purge configurator label.
   */
  public function label(): string;

  /**
   * Collects the entities that should be purged.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array containing the entities that should be purged.
   */
  public function collectEntities(): array;

  /**
   * Returns whether the given entity is allowed to be purged.
   *
   * This method is called immediately before the entity is deleted and allows
   * for cancelling purges of specific entities that otherwise match the
   * criteria used for collecting entities.
   *
   * For example, a purge configurator may collect for purging all log entities
   * older than 30 days; then use this method to prevent deleting logs that are
   * of level `critical` or higher to maintain a historical record of such logs.
   *
   * @param \Drupal\entity_purge\Entity\PurgeTypeInterface $purge_type
   *   The purge type that configured the purge requesting the entity purge.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being purged.
   *
   * @return bool
   *   Wheter purging the given entity is allowed or not.
   */
  public function allowEntityPurge(
    PurgeTypeInterface $purge_type,
    EntityInterface $entity,
  ): bool;

}
