<?php

namespace Drupal\entity_purge\Configurator;

use Drupal\entity_purge\Entity\PurgeTypeInterface;
use Drupal\Component\Plugin\PluginManagerInterface as ComponentPluginManagerInterface;

/**
 * Defines the interface for the purge configuration plugin manager.
 */
interface PluginManagerInterface extends ComponentPluginManagerInterface {

  /**
   * Creates an instance based on the plugin info of the given purge type.
   *
   * @param \Drupal\entity_purge\Entity\PurgeTypeInterface $purge_type
   *   The purge type.
   *
   * @return \Drupal\entity_purge\Configurator\PluginInterface
   *   The plugin.
   */
  public function createInstanceForPurgeType(
    PurgeTypeInterface $purge_type,
  ): PluginInterface;

}
