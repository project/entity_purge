<?php

declare(strict_types=1);

namespace Drupal\entity_purge\Configurator;

use Drupal\entity_purge\Entity\PurgeTypeInterface;
use Drupal\entity_purge\Exception\InvalidConfigurationException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase as CorePluginBase;

/**
 * Default base class for purge configurator plugin implementations.
 *
 * Supported configuration properties provided for all plugins extending this
 * class:
 * - scheduler: (string, optional, defaults to `scheduler`) The method used for
 *   scheduling/calling purge operations. Supported values are:
 *   - cron: Called upon cron run; all purge types configured by plugins with
 *     this option will be asked to collect entities for purging during this
 *     module's cron run.
 *   - custom: The provider is responsible for calling the purger service.
 *
 * Setting the scheduler to `custom` by default forces the provider to
 * intentionally choose a scheduler; this is to prevent accidentally scheduling
 * via cron when that is not the intended scheduler.
 */
abstract class PluginBase extends CorePluginBase implements PluginInterface {

  /**
   * Constructs a new PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Always include the default configuration.
    $this->setConfiguration($configuration);
    $this->validateConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [
      'module' => [$this->pluginDefinition['provider']],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * phpcs:disable
   * @I Support `ultimate_cron` scheduler
   *    type     : feature
   *    priority : normal
   *    labels   : scheduler
   *    notes    : Upon creating/editing, a job config entity should be created.
   *               If the scheduler is later changed, the job should be deleted.
   * phpcs:enable
   */
  public function defaultConfiguration() {
    return [
      'scheduler' => 'custom',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form['#tree'] = TRUE;

    $form['scheduler'] = [
      '#type' => 'radios',
      '#title' => $this->t('Scheduler'),
      '#description' => $this->t(
        'The scheduler responsible for calling the purger asking to perform a
         purge as determined by purge types configured by this plugin. Choosing
         Cron will run a purge every time cron runs for this module.',
      ),
      '#options' => [
        'custom' => $this->t('Custom'),
        'cron' => $this->t('Cron'),
      ],
      '#default_value' => $this->configuration['scheduler'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * Override this method if you need to do something specific to the
   * submitted data before it is saved as configuration on the plugin. The data
   * gets saved on the plugin in the purge type form.
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
    $this->configuration['scheduler'] = $form_state->getValue('scheduler');
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   *
   * By default, all collected entities are deleted.
   */
  public function allowEntityPurge(
    PurgeTypeInterface $purge_type,
    EntityInterface $entity,
  ): bool {
    return TRUE;
  }

  /**
   * Validates the plugin configuration.
   *
   * This method is called in the constructor so that errors in the plugin's
   * configuration are caught.
   *
   * @throws \Drupal\entity_purge\Exception\InvalidConfigurationException
   *   When the plugin is improperly configured.
   * phpcs:disable
   * @I Validate configuration upon import instead
   *    type     : bug
   *    priority : low
   *    labels   : config, configurator, validation
   * @I Support validating configuration property types e.g. string, int etc.
   *    type     : bug
   *    priority : low
   *    labels   : config, configurator, validation
   * phpcs:enable
   */
  protected function validateConfiguration(): void {
    // Nothing to validate by default. Plugin implementations may define
    // additional configuration properties which they can validate by overriding
    // this method.
  }

  /**
   * Validates that the given properties are present in the configuration.
   *
   * @param array $keys
   *   The required configuration property keys.
   *
   * @throws \Drupal\entity_purge\Exception\InvalidConfigurationException
   *   When one or more required properties are missing.
   */
  protected function validateConfigurationRequiredProperties(
    array $keys,
  ): void {
    $missing_keys = array_diff($keys, array_keys($this->configuration));
    if (!$missing_keys) {
      return;
    }

    throw new InvalidConfigurationException(sprintf(
      'The following configuration properties are required: %s',
      implode(', ', $missing_keys),
    ));
  }

}
