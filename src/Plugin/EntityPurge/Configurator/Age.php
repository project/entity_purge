<?php

declare(strict_types=1);

namespace Drupal\entity_purge\Plugin\EntityPurge\Configurator;

use Drupal\entity_purge\Configurator\PluginBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configurator that purges entities based on their age.
 *
 * Supported configuration properties:
 * - entity_type_id: (string, required) The type of the entities to purge.
 * - field_name: (string, required) The machine name of the field that will be
 *   used to calculate the age of the entity e.g. `created` or `changed`. It
 *   must be a timestamp field.
 * - min_age: (int, required) The minimum age in seconds that an entity must
 *   have in order to be collected for purging. For example, setting the minimum
 *   age to `2592000` will collect entities that their field contains a time
 *   that is earlier or equal to 30 days from the time of the request.
 * - limit: (int, required) The maximum number of entities to collect for
 *   purging on each run.

 * phpcs:disable
 * @EntityPurgeConfigurator(
 *   id = "age",
 *   label = @Translation("Age"),
 *   description = @Translation("Purges entities based on their age as determined by a timestamp field."),
 * )
 * @I Support `max_age` setting as well
 *    type     : improvement
 *    priority : low
 *    labels   : configurator
 * @I Support option for sorting when collecting the entities
 *    type     : improvement
 *    priority : low
 *    labels   : configurator
 * phpcs:enable
 */
class Age extends PluginBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new Age object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The system time.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected TimeInterface $time,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function collectEntities(): array {
    $storage = $this->entityTypeManager->getStorage(
      $this->configuration['entity_type_id'],
    );

    $max_time = $this->time->getRequestTime() - $this->configuration['min_age'];
    $entity_ids = $storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition($this->configuration['field_name'], $max_time, '<=')
      ->range(0, $this->configuration['limit'])
      ->execute();

    return $entity_ids ? $storage->loadMultiple($entity_ids) : [];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration(): void {
    $this->validateConfigurationRequiredProperties([
      'entity_type_id',
      'field_name',
      'min_age',
      'limit',
    ]);
  }

}
