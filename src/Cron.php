<?php

declare(strict_types=1);

namespace Drupal\entity_purge;

use Drupal\entity_purge\Entity\PurgeTypeInterface;
use Drupal\entity_purge\PurgerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * The default implementation of the cron service.
 *
 * It runs purges for all types that their configurator plugins use `cron` as
 * their scheduler.
 */
class Cron {

  /**
   * Constructs a new Cron object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\entity_purge\PurgerInterface $purger
   *   The entity purger.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected PurgerInterface $purger,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function run(): void {
    $purge_types = $this->entityTypeManager
      ->getStorage('entity_purge_type')
      ->loadMultipleWithPluginInstantiated();
    $purge_types = array_filter(
      $purge_types,
      fn ($p) => $p->getPluginConfiguration()['scheduler'] === 'cron',
    );

    $this->purger->run($purge_types);
  }

}
