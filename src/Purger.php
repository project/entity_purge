<?php

declare(strict_types=1);

namespace Drupal\entity_purge;

use Drupal\entity_purge\Entity\PurgeTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * The default implementation of the purger service.
 */
class Purger implements PurgerInterface {

  /**
   * Constructs a new Purger object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function run(array $purge_types): void {
    foreach ($purge_types as $purge_type) {
      $this->doPurge($purge_type);
    }
  }

  /**
   * Runs a purge operation for the given purge type.
   *
   * @param \Drupal\entity_purge\Entity\PurgeTypeInterface $purge_type
   *   The purge type for which to run the purge operation.
   * phpcs:disable
   * @I Allow plugins to implement their own purging methods
   *    type     : feature
   *    priority : normal
   *    labels   : plugin
   *    notes    : For example, to implement soft-deletion.
   * phpcs:enable
   */
  protected function doPurge(PurgeTypeInterface $purge_type): void {
    $plugin = $purge_type->getPlugin();
    $entities = $plugin->collectEntities($purge_type);

    foreach ($entities as $entity) {
      if (!$plugin->allowEntityPurge($purge_type, $entity)) {
        continue;
      }

      $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->delete([$entity]);
    }
  }

}
