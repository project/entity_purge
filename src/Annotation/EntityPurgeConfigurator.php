<?php

declare(strict_types=1);

namespace Drupal\entity_purge\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * The annotation object for the purge configurator plugins.
 *
 * Plugin namespace: `Plugin\EntityPurge\Configurator`.
 *
 * @Annotation
 * phpcs:disable
 * @I Add annotation property for restricting purging to specific entity types
 *    type     : feature
 *    priority : normal
 *    labels   : plugin
 * phpcs:enable
 */
class EntityPurgeConfigurator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $description;

}
