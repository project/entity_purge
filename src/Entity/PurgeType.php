<?php

declare(strict_types=1);

namespace Drupal\entity_purge\Entity;

use Drupal\entity_purge\Configurator\PluginInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * The default implementation of the Purge Type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "entity_purge_type",
 *   label = @Translation("Entity purge type"),
 *   label_singular = @Translation("entity purge type"),
 *   label_plural = @Translation("entity purge types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count entity purge type",
 *     plural = "@count entity purge types"
 *   ),
 *   admin_permission = "administer entity_purge_type",
 *   config_prefix = "type",
 *   handlers = {
 *     "storage" = "Drupal\entity_purge\Entity\Storage\PurgeType",
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "description",
 *     "plugin_id",
 *     "plugin_config",
 *   },
 * )
 */
class PurgeType extends ConfigEntityBase implements
  PurgeTypeInterface {

  /**
   * The machine name of this purge type.
   *
   * @var string
   */
  protected string $id;

  /**
   * The human-readable name of this purge type.
   *
   * @var string
   */
  protected string $label;

  /**
   * A brief description of this purge type.
   *
   * @var string
   */
  protected string|null $description;

  /**
   * The purge configurator plugin ID for this purge type.
   *
   * @var string
   */
  protected string $plugin_id;

  /**
   * The purge configurator plugin configuration for this purge type.
   *
   * @var array
   */
  protected array $plugin_config = [];

  /**
   * The purge configurator plugin for this purge type.
   *
   * @var \Drupal\entity_purge\Configurator\PluginInterface|null
   */
  protected PluginInterface|null $plugin;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginId(string $plugin_id): PurgeTypeInterface {
    $this->plugin_id = $plugin_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin(): PluginInterface|null {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlugin(PluginInterface $plugin): PurgeTypeInterface {
    $this->plugin = $plugin;
    $this->plugin_config = $plugin->getConfiguration();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfiguration(): array {
    return $this->plugin_config;
  }

}
