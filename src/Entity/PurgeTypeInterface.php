<?php

namespace Drupal\entity_purge\Entity;

use Drupal\entity_purge\Configurator\PluginInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for purge types.
 */
interface PurgeTypeInterface extends
  ConfigEntityInterface,
  EntityDescriptionInterface {

  /**
   * Returns the ID of the purge configurator plugin.
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId(): string;

  /**
   * Sets the ID of the purge configurator plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return $this
   */
  public function setPluginId(string $plugin_id): PurgeTypeInterface;

  /**
   * Returns the purge configurator plugin.
   *
   * @return \Drupal\entity_purge\Configurator\PluginInterface|null
   *   The plugin, or `NULL` if not instantiated yet.
   */
  public function getPlugin(): PluginInterface|null;

  /**
   * Sets the purge configurator plugin.
   *
   * It also updates the plugin configuration stored in the purge type.
   *
   * @param \Drupal\entity_purge\Configurator\PluginInterface $plugin
   *   The plugin.
   *
   * @return $this
   */
  public function setPlugin(PluginInterface $plugin): PurgeTypeInterface;

  /**
   * Returns the purge configurator plugin configuration.
   *
   * @return array
   *   The plugin configuration.
   */
  public function getPluginConfiguration(): array;

}
